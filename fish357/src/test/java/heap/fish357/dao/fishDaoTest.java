/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.dao;

import heap.fish357.model.Author;
import heap.fish357.model.Blog;
import heap.fish357.model.Category;
import heap.fish357.model.Tag;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author davidisakson
 */
public class fishDaoTest {

    fishDao dao;

    public fishDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("fishDao", fishDao.class);

        List<Author> auList = dao.getAllAuthor();
        for (Author au : auList) {
            dao.deleteAuthor(au.getAuthorID());
        }
        List<Tag> tagList = dao.getAllTag();
        for (Tag tag : tagList) {
            dao.deleteTag(tag.getTagID());
        }
        List<Category> catList = dao.getAllCategory();
        for (Category cat : catList) {
            dao.deleteCategory(cat.getCategoryID());
        }
        List<Blog> blogList = dao.getAllBlog();
        for(Blog blog : blogList){
            dao.deleteBlog(blog.getBlogID());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addAuthor method, of class fishDao.
     */
    @Test
    public void testAddGetAuthor() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Author daoAuthor = dao.getAuthorByID(author.getAuthorID());

        assertEquals(daoAuthor, author);
    }

    /**
     * Test of addPost method, of class fishDao.
     */
    @Test
    public void testAddGetPost() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);

        Blog daoBlog = dao.getBlogByID(blog.getBlogID());

        assertEquals(daoBlog, blog);
    }

    /**
     * Test of addCategory method, of class fishDao.
     */
    @Test
    public void testAddGetCategory() {
        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);

        dao.addCategory(cat);

        Category daoCat = dao.getCategoryByID(cat.getCategoryID());

        assertEquals(daoCat, cat);

    }

    /**
     * Test of addTag method, of class fishDao.
     */
    @Test
    public void testAddGetTag() {
        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");

        dao.addTag(tag);

        Tag daoTag = dao.getTagByID(tag.getTagID());

        assertEquals(daoTag, tag);
    }

    /**
     * Test of getAllBlog method, of class fishDao.
     */
    @Test
    public void testGetAllBlog() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);

        List<Blog> blogList = dao.getAllBlog();

        assertEquals(blogList.size(), 1);
    }

    /**
     * Test of getAllTag method, of class fishDao.
     */
    @Test
    public void testGetAllTag() {
        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");

        dao.addTag(tag);

        List<Tag> tagList = dao.getAllTag();
        assertEquals(tagList.size(), 1);

    }

    /**
     * Test of getAllCategory method, of class fishDao.
     */
    @Test
    public void testGetAllCategory() {
        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);

        dao.addCategory(cat);

        List<Category> catList = dao.getAllCategory();

        assertEquals(catList.size(), 1);
    }

    /**
     * Test of getAllAuthor method, of class fishDao.
     */
    @Test
    public void testGetAllAuthor() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);
    }

    /**
     * Test of deleteBlog method, of class fishDao.
     */
    @Test
    public void testDeleteBlog() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);
        
        dao.deleteBlog(blog.getBlogID());
        
        assertNull(dao.getBlogByID(blog.getBlogID()));
    }

    /**
     * Test of deleteTag method, of class fishDao.
     */
    @Test
    public void testDeleteTag() {
        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");

        dao.addTag(tag);

        dao.deleteTag(tag.getTagID());

        assertNull(dao.getTagByID(tag.getTagID()));
    }

    /**
     * Test of deleteAuthor method, of class fishDao.
     */
    @Test
    public void testDeleteAuthor() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        dao.deleteAuthor(author.getAuthorID());

        assertNull(dao.getAuthorByID(author.getAuthorID()));
    }

    /**
     * Test of deleteCategory method, of class fishDao.
     */
    @Test
    public void testDeleteCategory() {
        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);

        dao.addCategory(cat);

        dao.deleteCategory(cat.getCategoryID());

        assertNull(dao.getCategoryByID(cat.getCategoryID()));
    }

    /**
     * Test of updateBlog method, of class fishDao.
     */
    @Test
    public void testUpdateBlog() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);
        
        blog.setContent("updateContent");
        blog.setTitle("updateTitle");
        
        dao.updateBlog(blog);
        
        Blog updateBlog = dao.getBlogByID(blog.getBlogID());
        
        assertEquals(updateBlog, blog);
        
        
    }

    /**
     * Test of updateTag method, of class fishDao.
     */
    @Test
    public void testUpdateTag() {
        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");

        dao.addTag(tag);
        
        tag.setTagName("testUpdate");
        dao.updateTag(tag);
        
        Tag updateTag = dao.getTagByID(tag.getTagID());
        
        assertEquals(tag, updateTag);
    }

    /**
     * Test of updateCategory method, of class fishDao.
     */
    @Test
    public void testUpdateCategory() {
        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);

        dao.addCategory(cat);
        cat.setName("testUpdate");
        dao.updateCategory(cat);
        
        Category updateCat = dao.getCategoryByID(cat.getCategoryID());
        
        assertEquals(cat, updateCat);
    }

    /**
     * Test of updateAuthor method, of class fishDao.
     */
    @Test
    public void testUpdateAuthor() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);
        
        author.setFirstName("testUpdate");
        author.setAdmin(false);
        author.setLastName("testUpdate");
        
        dao.updateAuthor(author);
        
        Author updateAuthor = dao.getAuthorByID(author.getAuthorID());
        
        assertEquals(updateAuthor, author);
    }

    /**
     * Test of getRecentBlog method, of class fishDao.
     */
    @Test
    public void testGetRecentBlog() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);

        List<Blog> blogList = dao.getRecentBlog();

        assertEquals(blogList.size(), 1);
    }

    /**
     * Test of getBlogByTag method, of class fishDao.
     */
    @Test
    public void testGetBlogByTag() {
//        Author author = new Author();
//        author.setAdmin(true);
//        author.setAuthorID(0);
//        author.setFirstName("testFirst");
//        author.setLastName("testLast");
//
//        dao.addAuthor(author);
//
//        Category cat = new Category();
//        cat.setName("test");
//        cat.setTagID(0);
//        List<Category> catList = new ArrayList<>();
//        catList.add(cat);
//        dao.addCategory(cat);
//
//        Tag tag = new Tag();
//        tag.setTagID(0);
//        tag.setTagName("test");
//        List<Tag> tagList = new ArrayList<>();
//        dao.addTag(tag);
//
//        Blog blog = new Blog();
//        blog.setAuthor(author);
//        blog.setCatList(catList);
//        blog.setContent("testContent");
//        blog.setPostID(0);
//        blog.setTagList(tagList);
//        blog.setTitle("title");
//        blog.setViewCount(2);
//        blog.setUpdateDate(LocalDate.now());
//        blog.setPublishDate(LocalDate.now());
//        dao.addPost(blog);
//
//        List<Blog> blogList = dao.getBlogByTag(tag.getTagID());
//
//        assertEquals(blogList.size(), 1);
    }

    /**
     * Test of getBlogByCategory method, of class fishDao.
     */
    @Test
    public void testGetBlogByCategory() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);

        List<Blog> blogList = dao.getBlogByCategory(cat.getCategoryID());

        assertEquals(blogList.size(), 1);
    }

    /**
     * Test of getBlogByAuthor method, of class fishDao.
     */
    @Test
    public void testGetBlogByAuthor() {
//        Author author = new Author();
//        author.setAdmin(true);
//        author.setAuthorID(0);
//        author.setFirstName("testFirst");
//        author.setLastName("testLast");
//
//        dao.addAuthor(author);
//
//        Category cat = new Category();
//        cat.setName("test");
//        cat.setTagID(0);
//        List<Category> catList = new ArrayList<>();
//        catList.add(cat);
//        dao.addCategory(cat);
//
//        Tag tag = new Tag();
//        tag.setTagID(0);
//        tag.setTagName("test");
//        List<Tag> tagList = new ArrayList<>();
//        dao.addTag(tag);
//
//        Blog blog = new Blog();
//        blog.setAuthor(author);
//        blog.setCatList(catList);
//        blog.setContent("testContent");
//        blog.setPostID(0);
//        blog.setTagList(tagList);
//        blog.setTitle("title");
//        blog.setViewCount(2);
//        blog.setUpdateDate(LocalDate.now());
//        blog.setPublishDate(LocalDate.now());
//        dao.addPost(blog);
//
//        List<Blog> blogList = dao.getBlogByCategory(author.getAuthorID());
//        
//        assertEquals(1, blogList.size());
    }

    /**
     * Test of getArchivedBlog method, of class fishDao.
     */
    @Test
    public void testGetArchivedBlog() {
        Author author = new Author();
        author.setAdmin(true);
        author.setAuthorID(0);
        author.setFirstName("testFirst");
        author.setLastName("testLast");

        dao.addAuthor(author);

        Category cat = new Category();
        cat.setName("test");
        cat.setCategoryID(0);
        List<Category> catList = new ArrayList<>();
        catList.add(cat);
        dao.addCategory(cat);

        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        List<Tag> tagList = new ArrayList<>();
        dao.addTag(tag);

        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("testContent");
        blog.setBlogID(0);
        blog.setTagList(tagList);
        blog.setTitle("title");
        blog.setViewCount(2);
        blog.setUpdateDate(LocalDate.now());
        blog.setPublishDate(LocalDate.now());
        dao.addPost(blog);

        List<Blog> blogList = dao.getArchivedBlog();
        
        assertNotEquals(1, blogList.size());
    }

    /**
     * Test of approveBlog method, of class fishDao.
     */
    @Test
    public void testApproveBlog() {
//        Author author = new Author();
//        author.setAdmin(true);
//        author.setAuthorID(0);
//        author.setFirstName("testFirst");
//        author.setLastName("testLast");
//
//        dao.addAuthor(author);
//
//        Category cat = new Category();
//        cat.setName("test");
//        cat.setTagID(0);
//        List<Category> catList = new ArrayList<>();
//        catList.add(cat);
//        dao.addCategory(cat);
//
//        Tag tag = new Tag();
//        tag.setTagID(0);
//        tag.setTagName("test");
//        List<Tag> tagList = new ArrayList<>();
//        dao.addTag(tag);
//
//        Blog blog = new Blog();
//        blog.setAuthor(author);
//        blog.setCatList(catList);
//        blog.setContent("testContent");
//        blog.setPostID(0);
//        blog.setTagList(tagList);
//        blog.setTitle("title");
//        blog.setViewCount(2);
//        blog.setUpdateDate(LocalDate.now());
//        blog.setPublishDate(LocalDate.now());
//        dao.addPost(blog);
//        
//        dao.approveBlog(blog.getPostID());
//        
//        
    }

    /**
     * Test of changeTagToCat method, of class fishDao.
     */
    @Test
    public void testChangeTagToCat() {
        Tag tag = new Tag();
        tag.setTagID(0);
        tag.setTagName("test");
        
        dao.addTag(tag);
        
        dao.changeTagToCat(tag.getTagID());
        
        assertNull(dao.getTagByID(tag.getTagID()));
    }

    /**
     * Test of setPublishDate method, of class fishDao.
     */
    @Test
    public void testSetPublishDate() {

    }

}
