<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Carousel Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/index.css" rel="stylesheet" type="text/css">
        <link href="carousel.css" rel="stylesheet">
    </head>
    <!-- NAVBAR
    ================================================== -->
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}/">Home Page</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <form class="navbar-form navbar-right" action="login" method="GET">
                        <button type="submit" class="btn btn-success">Login</button>
                    </form>
                    
                        <form class="navbar-form navbar-right" action="displayAddBlog" method="GET">
                        <button type="submit" class="btn btn-success">Add Blog</button>
                    </form>    
                    <form class="navbar-form navbar-right" action="allBlogs" method="GET">
                        <button type="submit" class="btn btn-success">All Blogs</button>
                    </form>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>


        <!-- Carousel
        ================================================== -->

        <div class="jumbotron">
            <div class="container">
                <img src="">
                <h1 class="text-center">357 Fishin'</h1>
                <p>This is where we will either link or write our generic company information. The reason I am writing this right here is just to show what a little bit of example text would look like. We can have the LEARN MORE botton below link to a longer description.</p>
                <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
            </div>
        </div>


        <!--  Messaging and Featurettes
        ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->

        <div class="background-img container marketing">

            <!-- Three columns of text below the carousel -->



            <!-- START THE FEATURETTES -->

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">${blogOne.title}</h2>
                    <p class="blog-post-meta">${blogOne.publishDate} by <a href="#">${blogOne.author.firstName} ${blogOne.author.lastName}</a></p>
                    <div class="blog-post-content">
                        <p class="lead contentPreview">${blogOne.content}</p>
                    </div>                   
                    <p><a class="btn btn-info" href="displayblog?blogID=${blogOne.blogID}" role="button">Read More &raquo;</a></p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-responsive center-block blog-picture" src="images/default-img.gif" alt="Generic placeholder image">
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7 col-md-push-5">
                    <h2 class="featurette-heapding">${blogTwo.title}</h2>
                    <p class="blog-post-meta">${blogTwo.publishDate} by <a href="#">${blogTwo.author.firstName} ${blogTwo.author.lastName}</a></p>
                    <div class="blog-post-content">
                        <p class="lead contentPreview">${blogTwo.content}</p>
                    </div>
                    <p><a class="btn btn-info" href="displayblog?blogID=${blogTwo.blogID}" role="button">Read More &raquo;</a></p>
                </div>
                <div class="col-md-5 col-md-pull-7">
                    <img class="featurette-image img-responsive center-block blog-picture" src="images/default-img.gif" alt="Generic placeholder image">
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">${blogThree.title}</h2>
                    <p class="blog-post-meta">${blogThree.publishDate} by <a href="#">${blogThree.author.firstName} ${blogThree.author.lastName}</a></p>
                    <div class="blog-post-content">
                        <p class="lead contentPreview">${blogThree.content}</p>
                    </div>
                    <p><a class="btn btn-info" href="displayblog?blogID=${blogThree.blogID}" role="button">Read More &raquo;</a></p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-responsive center-block blog-picture" src="images/default-img.gif" alt="Generic placeholder image">
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row">
                <div class="col-lg-4">
                    <img class="img-circle" src="images/bass-master-tour.gif" alt="Generic placeholder image" width="140" height="140">
                    <h2>Bass Master Tour Package</h2>
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <img class="img-circle" src="images/night-tour.gif" alt="Generic placeholder image" width="140" height="140">
                    <h2>Guided Night Package</h2>
                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <img class="img-circle" src="images/pro-am-tour.gif" alt="Generic placeholder image" width="140" height="140">
                    <h2>Pro-Am Tour Package</h2>
                    <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->

            <hr class="featurette-divider">

            <!-- /END THE FEATURETTES -->


            <!-- FOOTER -->

            <footer>
                <p class="pull-right"><a href="#">Back to top</a></p>
                <p>&copy; 2017 357 Fishin', Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
            </footer>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
