<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Carousel Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/index.css" rel="stylesheet" type="text/css">
        <link href="carousel.css" rel="stylesheet">
    </head>
    <!-- NAVBAR
    ================================================== -->
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <form class="navbar-form navbar-right" action="loadIndex" method="GET">
                        <button type="submit" class="btn btn-primary" href="${pageContext.request.contextPath}/">Home</button>
                    </form>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <form class="navbar-form navbar-right" action="login" method="GET">
                        <button type="submit" class="btn btn-success">Login</button>
                    </form>
                    <form class="navbar-form navbar-right" action="allBlogs" method="GET">
                        <button type="submit" class="btn btn-success">All Blogs</button>
                    </form>
                    <form class="navbar-form navbar-right" action="displayBlog" method="GET">
                        <button type="submit" class="btn btn-primary">Blog</button>
                    </form>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>


        <!-- Carousel
        ================================================== -->

        <div class="jumbotron">
            <div class="container">
                <img src="">
                <h1 class="text-center">357 Fishin'</h1>
                <p>This is where we will either link or write our generic company information. The reason I am writing this right here is just to show what a little bit of example text would look like. We can have the LEARN MORE botton below link to a longer description.</p>
                <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
            </div>
        </div>


        <!--  Messaging and Featurettes
        ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->

        <div class="background-img container marketing">

            <div class="col-xs-8">
                <h3>Published Blogs:</h3>
                <div id="searchBlogs">
                    <c:forEach  var="blog" items="${blogList}">
                        <div class="">
                            <table class="blogTable table-bordered">
                                <tr>
                                    <th class="col-xs-4">Blog Title:</th>
                                    <th class="col-xs-4">Blog Preview:</th>
                                    <th class="col-xs-4">Blog Author:</th>
                                </tr>
                                <tr>
                                    <td>${blog.title}<br>
                                        <a href="displayblog?blogID=${blog.blogID}">Learn More</a>
                                    </td>
                                    <td><div style="height: 50px; overflow:scroll;">${blog.content}</div></td>
                                    <td>${blog.author.lastName}, ${blog.author.firstName}</td>
                                <tr>
                            </table>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="col-xs-4">
                <h2>Search:</h2>
                <select name="search" class="form-control" id="selectSearch">
                    <option value="tag">Search By Tag</option>
                    <option value="cat">Search By Category</option>
                    <option value="author">Search By Author</option>
                </select>
                <button class="btn btn-success" id="searchButton">Display Search</button>
                <div id="searchTags" style="display: none">
                    Tags Used:
                    <div>
                        <c:forEach var="tag" items="${tagList}">
                            <label class="checkbox-inline"><input type="radio" name="tags" value="${tag.tagID}">${tag.tagName}</label>
                            </c:forEach>
                            <button class="btn btn-primary" id="searchTagBtn">Tag Search</button>
                    </div>
                </div>
                <div id="searchAuthors" style="display: none">
                    Published Authors:
                    <div>
                        <c:forEach var="author" items="${authorList}">
                            <label class="checkbox-inline"><input type="radio" name="author" value="${author.authorID}">${author.lastName}, ${author.firstName}</label>
                            </c:forEach>
                            <button class="btn btn-primary" id="searchAuthorBtn">Author Search</button>
                    </div>
                </div>
                <div id="searchCats" style="display: none">
                    Categories Used:
                    <div>
                        <c:forEach var="cat" items="${catList}">
                            <label class="checkbox-inline"><input type="radio" name="cats" value="${cat.categoryID}">${cat.name}</label>
                            </c:forEach>
                            <button class="btn btn-primary" id="searchCatBtn">Category Search</button>
                    </div>
                </div>
            </div>


        </div><!-- /.container -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>
            <p>&copy; 2017 357 Fishin', Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>




        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="${pageContext.request.contextPath}/js/home.js" type="text/javascript"></script>
    </body>
</html>
