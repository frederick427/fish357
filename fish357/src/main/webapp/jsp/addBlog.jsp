<%-- 
    Document   : addBlog
    Created on : Apr 4, 2017, 3:09:41 PM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/addBlogStyles.css" rel="stylesheet" type="text/css">

        <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
        <script>
            tinymce.init({
                selector: 'textarea', // change this value according to your HTML
                toolbar: 'bullist, numlist',
                plugins: 'lists advlist code',
                advlist_bullet_styles: 'square',
                advlist_number_styles: 'lower-alpha,lower-roman,upper-alpha,upper-roman',
                width: 800,
                height: 500
            });
        </script>
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Home Page</a>
                    <c:if test="${pageContext.request.userPrincipal.name != null}">
                        <h4>Hello : ${pageContext.request.userPrincipal.name}
                            | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                        </h4>
                    </c:if>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <form class="navbar-form navbar-right" action="loadIndex" method="GET">
                        <button type="submit" class="btn btn-primary" href="${pageContext.request.contextPath}/">Home</button>
                    </form>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>
        <br/>
        <br/>
        <div class="jumbotron">
            <div class="container">
                <form id="addBlogID" class="form-horizontal" role="form" method="POST" action="addBlog">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Title:</label>
                            <div class="col-md-4">
                                <input name="title" class="form-control" type="text"/>
                            </div>
                            <label class="col-md-2 text-right" for="selectAuthor">Select Author:</label>
                            <div class="col-md-4">
                                <select name="author" class="form-control" id="selectAuthor">
                                    <c:forEach var="author" items="${authorList}">
                                        <option value="${author.authorID}">${author.lastName}, ${author.firstName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Tags:</label>
                            <div class="col-md-4">
                                <label>Select Tags: (hold ctrl or shift (or drag with the mouse) to select more than one)</label>
                                <div id="tagReload" class="container">
                                    <c:forEach var="tag" items="${tagList}">
                                        <label class="checkbox-inline"><input type="checkbox" name="tags" value="${tag.tagID}">${tag.tagName}</label>
                                        </c:forEach>
                                </div>
                                <div>
                                    <div id="addTagButtonContainer">
                                        <button id="add-tag-button" type="button" class="btn">Add Tag</button><br><br>
                                    </div>
                                    <div id="addTagFormContainer" style="display:none">
                                        <label>Tag Name: <input id="tagName" type="text"></label>
                                        <button id="submit-tag-button" type="button" class="btn">Submit Tag</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Categories:</label>
                            <div class="col-md-4">
                                <label>Select Categories: (hold ctrl or shift (or drag with the mouse) to select more than one)</label>
                                <div id="catReload" class="container">
                                    <c:forEach var="cat" items="${catList}">
                                        <label class="checkbox-inline"><input type="checkbox" name="cats" value="${cat.categoryID}">${cat.name}</label>
                                        </c:forEach>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="addCategoryButtonContainer">
                                    <button id="add-category-button" type="button" class="btn">Add Category</button><br><br>
                                </div>
                                <div id="addCategoryFormContainer" style="display:none">
                                    <label>Category Name: <input id="catName" type="text"></label>
                                    <button id="submit-category-button" type="button" class="btn">Submit Category</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <textarea name="content" id="textarea"><p>Hello, World!</p></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <button id="save-blog-button" type="button" class="btn">Save Blog</button>
                            <button id="submit-blog-button" type="submit" class="btn">Submit Blog</button>
                            <br><br><br>
                        </div>

                    </div>
                </form>
            </div>
            <!-- FOOTER -->
            <div class="container marketing">
                <footer>
                    <p class="pull-right"><a href="#">Back to top</a></p>
                    <p>&copy; 2017 357 Fishin', Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
                </footer>
            </div> <!-- End of Container -->
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/home.js" type="text/javascript"></script>
    </body>
</html>
