<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/home.css" rel="stylesheet">
        <title>Blog Page</title>
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-9">
                        <h1>357 Fishin'</h1>
                    </div>

                    <div class="col-md-3">
                        <button action="login" method="GET">
                            <a href="${pageContext.request.contextPath}/login" value="Login">Login</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
                        <div>
                            <a href="${pageContext.request.contextPath}/displayAddBlog">Add Blog</a>
                        <!--    <a href="displayblog?blogID=7">Sample blog patch</a> -->
                        </div>
                        <c:forEach  var="blog" items="${blogList}"> 
                            <div class="">
                                <div><h2>${blog.title}</h2></div>
                                <div>${blog.content}</div>
                                <a href="displayblog?blogID=${blog.blogID}">Learn More</a>
                            </div>
                        </c:forEach>
                        
        <footer class="container-fluid text-center">
            <div class="container">
                <div id="footer">
                    <div class="row">
                        <div class="col-md-1">
                            <a href="" value="privacy">Privacy</a>
                        </div>
                        <div class="col-md-1">
                            <a href="" value="Information">Info</a> 
                        </div>

                    </div>
                </div>
            </div>
        </footer>
        <style>
            #footer {
                position:absolute;
                bottom:0;
                width:100%;
                height:60px;   /* Height of the footer */
                background:#0000;
                text-align: center;
            }
        </style>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>