<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/home.css" rel="stylesheet">
        <title>Blog Page</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-11">
                        <h1 class="text-center">357 Fishin'</h1>
                    </div>

                    <div class="col-md-1">
                        <button action="login" method="GET">
                            <a href="${pageContext.request.contextPath}/login" value="Login">Login</a>
                        </button>
                    </div>
                </div>
            </div>
            <div class="body">
                <h3>Title:<c:out value="${blog.title}"/></h3>
                <h4>Author:<c:out value="${blog.author.lastName}"/></h4>
                <div>Categories:</div>
                <c:forEach var="cat" items="${blog.catList}">
                    <div>${cat.name}</div>   
                </c:forEach>
                <div name="bodyOfBlog" class="col-md-12">
                    ${blog.content}
                </div>
                <div>Tags:</div>
                <div>
                <c:forEach var="tag" items="${blog.tagList}">
                    <div>${tag.tagName}</div>   
                </c:forEach>
                </div>
            </div>
        </div>
        <footer class="container-fluid text-center">
            <div class="container">

                <div id="footer">
                    <div class="row">
                        <div class="col-md-1">
                            <a href="" value="privacy">Privacy</a>
                        </div>
                        <div class="col-md-1">
                            <a href="" value="Information">Info</a> 
                        </div>
                        
                    </div>
                </div>
            </div>

        </footer>
        <style>
            #footer {
                position:absolute;
                bottom:0;
                width:100%;
                height:60px;   /* Height of the footer */
                background:#0000;
                text-align: center;
            }
        </style>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>

