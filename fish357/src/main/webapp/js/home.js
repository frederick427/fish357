$("#add-tag-button").click(function(){
    $("#addTagFormContainer").toggle();
});
$("#add-category-button").click(function(){
    $("#addCategoryFormContainer").toggle();
});




$("#submit-tag-button").click(function(){
    
var tagName = $("#tagName").val();
if(tagName.length > 0){
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/fish357/tag/'+tagName,
        success: function (tagResponse) {
        $("#tagReload").empty();   
        var reloader = '';
        $.each(tagResponse, function(index, tag) {
        reloader += '<label class="checkbox-inline">';
        reloader += '<input type="checkbox" name="tags" value="'+tag.tagID+'">'+tag.tagName+'</label>';
        });
        $("#tagReload").append(reloader);
        },
        error: function (tagResponse) {
            alert("Tag Not Connecting");
        }

    });

}
else {
    alert("please enter a tag")
}
});
$("#submit-category-button").click(function(){
    
    var catName = $("#catName").val();
    if(catName.length > 0){
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/fish357/category/'+catName,
        success: function (catResponse) {
        $("#catReload").empty();   
        var reloader = '';
        $.each(catResponse, function(index, cat) {
        reloader += '<label class="checkbox-inline">';
        reloader += '<input type="checkbox" name="tags" value="'+cat.categoryID+'">'+cat.name+'</label>';
        });
        $("#catReload").append(reloader);
        },
        error: function (catResponse) {
            alert("Categories Not Connecting");
        }

    });

}
else {
    alert("please enter a category")
}
});
$("#searchButton").click(function(){
    var choice = $("#selectSearch").val();
    
    if(choice === "tag"){
        $("#searchTags").show();
        $("#searchCats").hide();
        $("#searchAuthors").hide();
    }else if(choice === "cat"){
        $("#searchCats").show();
        $("#searchTags").hide();
        $("#searchAuthors").hide();
    }else if(choice === "author"){
        $("#searchAuthors").show();
        $("#searchTags").hide();
        $("#searchCats").hide();
    }
});

