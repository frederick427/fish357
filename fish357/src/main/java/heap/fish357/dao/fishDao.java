/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.dao;

import heap.fish357.model.Author;
import heap.fish357.model.Category;
import heap.fish357.model.Blog;
import heap.fish357.model.Tag;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author davidisakson
 */
public interface fishDao {
    public void addAuthor(Author author);
    
    public void addPost(Blog blog);
    
    public void addCategory(Category category);
    
    public void addTag(Tag tag);
    
    public List<Blog> getAllBlog();
    
    public List<Tag> getAllTag();
    
    public List<Category> getAllCategory();
    
    public List<Author> getAllAuthor();
    
    
    public Tag getTagByID(int tagID);
    
    public Category getCategoryByID(int catID);
    
    public Author getAuthorByID(int authorID);
    
    public Blog getBlogByID(int blogID);
    
    
    public void deleteBlog(int blogID);
    
    public void deleteTag(int tagID);
    
    public void deleteAuthor(int authorID);
    
    public void deleteCategory(int categoryID);
    
    
    public void updateBlog(Blog blog);
    
    public void updateTag(Tag tag);
    
    public void updateCategory(Category category);
    
    public void updateAuthor(Author author);
    
    
    public List<Blog> getRecentBlog();
    
    public List<Blog> getBlogByTag(int tagID);
    
    public List<Blog> getBlogByCategory(int catID);
    
    public List<Blog> getBlogByAuthor(int authroID);
    
    public List<Blog> getArchivedBlog();
    
    public void approveBlog(int blogID);
    
    public void changeTagToCat(int catID);
    
    public void setPublishDate(int blogID, LocalDate date);
    
    public List<Blog> getBlogNotApproved();
    
}
