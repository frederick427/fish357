/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.dao;

import heap.fish357.model.Author;
import heap.fish357.model.Blog;
import heap.fish357.model.Category;
import heap.fish357.model.Tag;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author davidisakson
 */
public class fishDaoDbImpl implements fishDao{
    
    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
    private static final String SQL_INSERT_AUTHOR = "insert into Author (FirstName, LastName, Admin) "
            + "values (?,?,?)";
    private static final String SQL_INSERT_POST_CATEGORY = "insert into PostCategory (PostID, CategoryID) "
            + "values (?, ?)";
    private static final String SQL_INSERT_CATEGORY = "insert into Category (CategoryName, Tag) values (?,?)";
    private static final String SQL_INSERT_POST = "insert into Post (Title, Content, AuthorID, ViewCount, PublishDate, UpdateDate) "
            + " values (?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_DELETE_AUTHOR = "update Author set DeleteAuthor = true where AuthorID = ?";
    private static final String SQL_DELETE_CATEGORY = "update Category set DeleteCategory = true where CategoryID = ?";
    private static final String SQL_DELETE_POST = "update Post set DeletePost = true where PostID = ?";
    private static final String SQL_DELETE_POST_CATEGORY = "delete from PostCategory where PostID = ?";
    
    private static final String SQL_SELECT_AUTHOR = "select * from Author where AuthorID = ? and DeleteAuthor = false";
    private static final String SQL_SELECT_CATEGORY = "select * from Category where CategoryID = ? and DeleteCategory = false and Tag = false";
    private static final String SQL_SELECT_POST = "select * from Post where PostID = ? and DeletePost = false";
    private static final String SQL_SELECT_TAG = "select * from Category where CategoryID = ? and DeleteCategory = false and Tag = True";
    
    private static final String SQL_SELECT_ALL_AUTHOR = "select * from Author where DeleteAuthor = false";
    private static final String SQL_SELECT_ALL_CATEGORY = "select * from Category where DeleteCategory = false and Tag = false";
    private static final String SQL_SELECT_ALL_POST = "select * from Post where DeletePost = false";
    private static final String SQL_SELECT_ALL_TAG = "select * from Category where DeleteCategory = false and Tag = True";
    
    private static final String SQL_UPDATE_AUTHOR= "update Author set FirstName = ?, LastName = ?, Admin = ? where "
            + "AuthorID = ?";
    private static final String SQL_UPDATE_CATEGORY = "update Category set CategoryName = ?, Tag = ? where CategoryID = ?";
    private static final String SQL_UPDATE_POST = "update Post set Title = ?, Content = ?, AuthorID = ?, ViewCount = ?, UpdateDate = ?, Approved = ?"
            + " where PostID = ?";
    
    private static final String SQL_GET_RECENT_BLOG = "select * from Post where DeletePost = false order by PostID desc Limit 3";
    private static final String SQL_GET_BLOG_BY_TAG = "select p.UpdateDate,p.PostID, p.Title, p.Content, p.AuthorID, p.ViewCount, p.PublishDate"
            + " from Category c inner join PostCategory cp on c.CategoryID = cp.CategoryID inner join Post p"
            + " on p.PostID = cp.PostID where c.CategoryID = ? and c.Tag = true and p.DeletePost = false";
    private static final String SQL_GET_BLOG_BY_CATEGORY = "select p.UpdateDate, p.PostID, p.Title, p.Content, p.AuthorID, p.ViewCount, p.PublishDate"
            + " from Category c inner join PostCategory cp on c.CategoryID = cp.CategoryID inner join Post p"
            + " on p.PostID = cp.PostID where c.CategoryID = ? and c.Tag = false and p.DeletePost = false";
    private static final String SQL_GET_BLOG_BY_AUTHOR = "select p.PostID, p.Title, p.Content, p.AuthorID, p.ViewCount, p.PublishDate"
            + " from Post p where p.DeletePost = false";
    private static final String SQL_GET_BLOG_NOT_APPRIVED = "select * from Post where Approved = false and DeletePost = false";
    private static final String SQL_SELECT_EVERY_POST = "select * from Post";
    private static final String SQL_APPROVE_BLOG = "update Post set Approved = true where PostID = ?";
    private static final String SQL_CHANGE_TAG_TO_CAT = "update Category set Tag = false where CategoryID = ?";
    private static final String SQL_SET_PUBLISH_DATE = "update Post set PublishDate = ? where PostID = ?";
    private static final String SQL_GET_TAG_BY_BLOG = "select c.CategoryID, c.CategoryName"
            + " from Category c inner join PostCategory cp on c.CategoryID = cp.CategoryID inner join Post p"
            + " on p.PostID = cp.PostID where p.PostID = ? and c.Tag = true and c.DeleteCategory = false";
    private static final String SQL_GET_CAT_BY_BLOG = "select c.CategoryID, c.CategoryName"
            + " from Category c inner join PostCategory cp on c.CategoryID = cp.CategoryID inner join Post p"
            + " on p.PostID = cp.PostID where p.PostID = ? and c.Tag = false and c.DeleteCategory = false";
    private static final String SQL_GET_AUTHOR_BY_BLOG = "select a.AuthorID, a.FirstName, a.LastName, a.Admin from"
            + " Author a inner join Post p on a.AuthorID = p.AuthorID where PostID = ?";
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addAuthor(Author au){
        jdbcTemplate.update(SQL_INSERT_AUTHOR,au.getFirstName(), au.getLastName(), au.isAdmin());
        int authorID = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        au.setAuthorID(authorID);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addPost(Blog blog){
        Date date = Date.valueOf(blog.getPublishDate());
        Date date2 = Date.valueOf(blog.getUpdateDate());
        jdbcTemplate.update(SQL_INSERT_POST, 
                blog.getTitle(),
                blog.getContent(),
                blog.getAuthor().getAuthorID(),
                blog.getViewCount(),
                date, date2);
        blog.setBlogID(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        
        insertBlogsTagsAndCat(blog);
    }
    
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addCategory(Category cat){
        jdbcTemplate.update(SQL_INSERT_CATEGORY, cat.getName(), false);
        int catID = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        cat.setCategoryID(catID);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addTag(Tag tag){
        jdbcTemplate.update(SQL_INSERT_CATEGORY, tag.getTagName(), true);
        int tagID = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        tag.setTagID(tagID);
    }      
    @Override
    public List<Blog> getAllBlog() {
        List<Blog> blogList = jdbcTemplate.query(SQL_SELECT_ALL_POST, new BlogMapper());
        
        return associateCatTagAndAuthor(blogList);
    }

    @Override
    public List<Tag> getAllTag() {
        return jdbcTemplate.query(SQL_SELECT_ALL_TAG, new TagMapper());
    }

    @Override
    public List<Category> getAllCategory() {
        return jdbcTemplate.query(SQL_SELECT_ALL_CATEGORY, new CategoryMapper());
    }
    
    @Override
    public List<Author> getAllAuthor() {
        return jdbcTemplate.query(SQL_SELECT_ALL_AUTHOR, new AuthorMapper());
    }

    @Override
    public Tag getTagByID(int tagID) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_TAG, new TagMapper(), tagID);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Category getCategoryByID(int catID) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY, new CategoryMapper(), catID);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Author getAuthorByID(int authorID) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_AUTHOR, new AuthorMapper(), authorID);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Blog getBlogByID(int blogID) {
        try{
            Blog blog = jdbcTemplate.queryForObject(SQL_SELECT_POST, new BlogMapper(), blogID);
            blog.setAuthor(findAuthorForBlog(blog));
            blog.setCatList(findCategoryForBlog(blog));
            blog.setTagList(findTagForBlog(blog));
            return blog;
        }catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteBlog(int blogID) {
        jdbcTemplate.update(SQL_DELETE_POST, blogID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteTag(int tagID) {
        jdbcTemplate.update(SQL_DELETE_CATEGORY, tagID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteAuthor(int authorID) {
        jdbcTemplate.update(SQL_DELETE_AUTHOR, authorID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteCategory(int categoryID) {
        jdbcTemplate.update(SQL_DELETE_CATEGORY, categoryID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateBlog(Blog blog) {
        Date date = Date.valueOf(blog.getPublishDate());
        jdbcTemplate.update(SQL_UPDATE_POST,
                blog.getTitle(),
                blog.getContent(),
                blog.getAuthor().getAuthorID(),
                blog.getViewCount(),
                date,
                false,
                blog.getBlogID());
        jdbcTemplate.update(SQL_DELETE_POST_CATEGORY, blog.getBlogID());
        insertBlogsTagsAndCat(blog);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateTag(Tag tag) {
        jdbcTemplate.update(SQL_UPDATE_CATEGORY, tag.getTagName(), true, tag.getTagID());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateCategory(Category category) {
        jdbcTemplate.update(SQL_UPDATE_CATEGORY, category.getName(), false, category.getCategoryID());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateAuthor(Author au) {
        jdbcTemplate.update(SQL_UPDATE_AUTHOR, 
                au.getFirstName(),
                au.getLastName(), 
                au.isAdmin(), 
                au.getAuthorID());
    }

    @Override
    public List<Blog> getRecentBlog() {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_RECENT_BLOG, new BlogMapper());
        
        return associateCatTagAndAuthor(blogList);
    }

    @Override
    public List<Blog> getBlogByTag(int tagID) {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_BLOG_BY_TAG, new BlogMapper(), tagID);
        
        return associateCatTagAndAuthor(blogList);
    }
    @Override
    public List<Blog> getBlogNotApproved(){
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_BLOG_NOT_APPRIVED, new BlogMapper());
        
        return associateCatTagAndAuthor(blogList);
    }
    
    public List<Blog> getBlogByCategory(int catID){
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_BLOG_BY_CATEGORY, new BlogMapper(), catID);
        
        return associateCatTagAndAuthor(blogList);
    }

    @Override
    public List<Blog> getBlogByAuthor(int authroID) {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_BLOG_BY_AUTHOR, new BlogMapper());
        
        return associateCatTagAndAuthor(blogList);
    }

    @Override
    public List<Blog> getArchivedBlog() {
        List<Blog> blogList = jdbcTemplate.query(SQL_SELECT_EVERY_POST, new BlogMapper());
        
        return associateCatTagAndAuthor(blogList);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void approveBlog(int blogID) {
        jdbcTemplate.update(SQL_APPROVE_BLOG, blogID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void changeTagToCat(int catID) {
        jdbcTemplate.update(SQL_CHANGE_TAG_TO_CAT, catID);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void setPublishDate(int blogID, LocalDate date) {
        jdbcTemplate.update(SQL_SET_PUBLISH_DATE, date, blogID);
    }
    
    private static final class AuthorMapper implements RowMapper<Author> {
        
        @Override
        public Author mapRow(ResultSet rs, int i) throws SQLException {
            Author au = new Author();
            au.setAuthorID(rs.getInt("AuthorID"));
            au.setFirstName(rs.getString("FirstName"));
            au.setLastName(rs.getString("LastName"));
            au.setAdmin(rs.getBoolean("Admin"));
            
            return au;
        }
    }
    private static final class TagMapper implements RowMapper<Tag> {
        
        @Override
        public Tag mapRow(ResultSet rs, int i) throws SQLException {
            Tag tag = new Tag();
            tag.setTagID(rs.getInt("CategoryID"));
            tag.setTagName(rs.getString("CategoryName"));
            return tag;
        }
    }
    private static final class CategoryMapper implements RowMapper<Category> {
        
        @Override
        public Category mapRow(ResultSet rs, int i) throws SQLException {
            Category cat = new Category();
            cat.setCategoryID(rs.getInt("CategoryID"));
            cat.setName(rs.getString("CategoryName"));
            return cat;
        }
    }
    private static final class BlogMapper implements RowMapper<Blog> {
        
        @Override
        public Blog mapRow(ResultSet rs, int i) throws SQLException {
            Blog blog = new Blog();
            blog.setBlogID(rs.getInt("PostID"));
            blog.setContent(rs.getString("Content"));
            blog.setViewCount(rs.getInt("ViewCount"));
            blog.setTitle(rs.getString("Title"));
            blog.setPublishDate(
                    Instant.ofEpochMilli((rs.getDate("PublishDate")).getTime()).atZone(ZoneId.systemDefault()).toLocalDate()
                            );
            blog.setUpdateDate(
                    Instant.ofEpochMilli((rs.getDate("UpdateDate")).getTime()).atZone(ZoneId.systemDefault()).toLocalDate()
                            );;
            
            return blog;
            
        }
    }
    private void insertBlogsTagsAndCat(Blog blog) {
        final int blogID = blog.getBlogID();
        final List<Tag> tagList = blog.getTagList();
        final List<Category> catList = blog.getCatList();
        
        for (Tag tag : tagList){
            jdbcTemplate.update(SQL_INSERT_POST_CATEGORY,blogID, tag.getTagID());
        }
        for(Category cat : catList){
            jdbcTemplate.update(SQL_INSERT_POST_CATEGORY,blogID, cat.getCategoryID());
        }
    }
    private List<Category> findCategoryForBlog(Blog blog){
        return jdbcTemplate.query(SQL_GET_CAT_BY_BLOG, new CategoryMapper(), blog.getBlogID());
    }
    private List<Tag> findTagForBlog(Blog blog){
        return jdbcTemplate.query(SQL_GET_TAG_BY_BLOG, new TagMapper(), blog.getBlogID());
    }
    private Author findAuthorForBlog(Blog blog){
        return jdbcTemplate.queryForObject(SQL_GET_AUTHOR_BY_BLOG, new AuthorMapper(), blog.getBlogID());
    }
    
    private List<Blog> associateCatTagAndAuthor(List<Blog> blogList){
        
        for (Blog blog : blogList){
            blog.setAuthor(findAuthorForBlog(blog));
            blog.setCatList(findCategoryForBlog(blog));
            blog.setTagList(findTagForBlog(blog));
        }
       return blogList;
    }
    
    
}
