/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.dao;

import heap.fish357.model.Author;
import heap.fish357.model.Blog;
import heap.fish357.model.Category;
import heap.fish357.model.Tag;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author davidisakson
 */
public class fishDaoInMemImpl implements fishDao{
    private List<Category> catList = new ArrayList<>();
    private List<Tag> tagList = new ArrayList<>();
    private List<Blog> blogList = new ArrayList<>();
    private List<Author> authorList = new ArrayList<>();
    
    public fishDaoInMemImpl(){
        Tag tag = new Tag();
        tag.setTagID(1);
        tag.setTagName("testTag");
        tagList.add(tag);
        
        Category cat = new Category();
        cat.setCategoryID(1);
        cat.setName("testCat");
        catList.add(cat);
        
        Author author = new Author();
        author.setAuthorID(1);
        author.setFirstName("testFirst");
        author.setLastName("testLast");
        author.setAdmin(true);
        authorList.add(author);
        
        Blog blog = new Blog();
        blog.setAuthor(author);
        blog.setCatList(catList);
        blog.setContent("<p>Sample html content</p>");
        blog.setBlogID(1);
        blog.setPublishDate(LocalDate.now());
        blog.setTitle("TestTitle");
        blog.setTagList(tagList);
        blogList.add(blog);
    };
    
    @Override
    public List<Blog> getAllBlog() {
        return blogList;
    }

    @Override
    public List<Tag> getAllTag() {
        return tagList;
    }

    @Override
    public List<Category> getAllCategory() {
        return catList;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorList;
    }

    @Override
    public Tag getTagByID(int tagID) {
        return tagList.get(tagID);
    }

    @Override
    public Category getCategoryByID(int catID) {
        return catList.get(catID);
    }

    @Override
    public Author getAuthorByID(int authorID) {
        return authorList.get(authorID);
    }

    @Override
    public Blog getBlogByID(int blogID) {
        return blogList.get(blogID);
    }

    @Override
    public void deleteBlog(int blogID) {
        blogList.remove(blogID);
    }

    @Override
    public void deleteTag(int tagID) {
        tagList.remove(tagID);
    }

    @Override
    public void deleteAuthor(int authorID) {
        authorList.remove(authorID);
    }

    @Override
    public void deleteCategory(int categoryID) {
        catList.remove(categoryID);
    }

    @Override
    public void updateBlog(Blog blog) {
        blogList.set(blog.getBlogID(), blog);
    }

    @Override
    public void updateTag(Tag tag) {
        tagList.set(tag.getTagID(), tag);
    }

    @Override
    public void updateCategory(Category category) {
        catList.set(category.getCategoryID(), category);
    }

    @Override
    public void updateAuthor(Author author) {
        authorList.set(author.getAuthorID(), author);
    }

    @Override
    public List<Blog> getRecentBlog() {
        return blogList;
    }

    @Override
    public List<Blog> getBlogByTag(int tagID) {
        return blogList;
    }

    @Override
    public List<Blog> getBlogByAuthor(int authorID) {
        return blogList;
    }

    @Override
    public List<Blog> getArchivedBlog() {
        return blogList;
    }

    @Override
    public void approveBlog(int blogID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeTagToCat(int catID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPublishDate(int blogID, LocalDate date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addAuthor(Author author) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addPost(Blog blog) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCategory(Category category) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTag(Tag tag) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Blog> getBlogByCategory(int catID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Blog> getBlogNotApproved() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
