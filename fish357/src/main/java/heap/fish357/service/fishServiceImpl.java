/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.service;

import heap.fish357.dao.fishDao;
import heap.fish357.model.Author;
import heap.fish357.model.Blog;
import heap.fish357.model.Category;
import heap.fish357.model.Tag;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author davidisakson
 */
public class fishServiceImpl implements fishService{
    
    fishDao dao;
    
    public fishServiceImpl(fishDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Blog> getAllBlog() {
        return dao.getAllBlog();
    }

    @Override
    public List<Tag> getAllTag() {
        return dao.getAllTag();
    }

    @Override
    public List<Category> getAllCategory() {
        return dao.getAllCategory();
    }

    @Override
    public List<Author> getAllAuthor() {
        return dao.getAllAuthor();
    }

    @Override
    public Tag getTagByID(int tagID) {
        return dao.getTagByID(tagID);
    }

    @Override
    public Category getCategoryByID(int catID) {
        return dao.getCategoryByID(catID);
    }

    @Override
    public Author getAuthorByID(int authorID) {
        return dao.getAuthorByID(authorID);
    }

    @Override
    public Blog getBlogByID(int blogID) {
        return dao.getBlogByID(blogID);
    }

    @Override
    public void deleteBlog(int blogID) {
        dao.deleteBlog(blogID);
    }

    @Override
    public void deleteTag(int tagID) {
        dao.deleteTag(tagID);
    }

    @Override
    public void deleteAuthor(int authorID) {
        dao.deleteAuthor(authorID);
    }

    @Override
    public void deleteCategory(int categoryID) {
        dao.deleteCategory(categoryID);
    }

    @Override
    public Blog updateBlog(Blog blog) {
        dao.updateBlog(blog);
        return blog;
    }

    @Override
    public Tag updateTag(Tag tag) {
        dao.updateTag(tag);
        return tag;
    }

    @Override
    public Category updateCategory(Category category) {
        dao.updateCategory(category);
        return category;
    }

    @Override
    public Author updateAuthor(Author author) {
        dao.updateAuthor(author);
        return author;
    }

    @Override
    public List<Blog> getRecentBlog() {
        return dao.getRecentBlog();
    }

    @Override
    public List<Blog> getArchivedBlog() {
        return dao.getArchivedBlog();
    }

    @Override
    public List<Blog> getBlogByTag(int tagID) {
        return dao.getBlogByTag(tagID);
    }

    @Override
    public List<Blog> getBlogByCategory(int catID) {
        return dao.getBlogByCategory(catID);
    }

    @Override
    public List<Blog> getBlogByAuthor(int authroID) {
        return dao.getBlogByAuthor(authroID);
    }

    @Override
    public void approveBlog(int blogID) {
        dao.approveBlog(blogID);
    }

    @Override
    public void changeTagToCat(int catID) {
        dao.changeTagToCat(catID);
    }

    @Override
    public void setPublishDate(int blogID, LocalDate date) {
        dao.setPublishDate(blogID, date);
    }

    @Override
    public List<Blog> getBlogNotApproved() {
        return dao.getBlogNotApproved();
    }

    @Override
    public void addAuthor(Author author) {
        dao.addAuthor(author);
    }

    @Override
    public void addPost(Blog blog) {
        dao.addPost(blog);
    }

    @Override
    public void addCategory(Category category) {
        dao.addCategory(category);
    }

    @Override
    public void addTag(Tag tag) {
        dao.addTag(tag);
    }
    
}
