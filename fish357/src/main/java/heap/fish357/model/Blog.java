/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author davidisakson
 */
public class Blog {
    private int blogID;
    private String title;
    private String content;
    private Author author;
    private int viewCount;
    private LocalDate publishDate;
    private LocalDate updateDate;
    private List<Tag> tagList;
    private List<Category> catList;

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int postID) {
        this.blogID = postID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Category> getCatList() {
        return catList;
    }

    public void setCatList(List<Category> catList) {
        this.catList = catList;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.blogID;
        hash = 37 * hash + Objects.hashCode(this.title);
        hash = 37 * hash + Objects.hashCode(this.content);
        hash = 37 * hash + Objects.hashCode(this.author);
        hash = 37 * hash + this.viewCount;
        hash = 37 * hash + Objects.hashCode(this.publishDate);
        hash = 37 * hash + Objects.hashCode(this.updateDate);
        hash = 37 * hash + Objects.hashCode(this.tagList);
        hash = 37 * hash + Objects.hashCode(this.catList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Blog other = (Blog) obj;
        if (this.blogID != other.blogID) {
            return false;
        }
        if (this.viewCount != other.viewCount) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        if (!Objects.equals(this.publishDate, other.publishDate)) {
            return false;
        }
        if (!Objects.equals(this.updateDate, other.updateDate)) {
            return false;
        }
        if (!Objects.equals(this.tagList, other.tagList)) {
            return false;
        }
        if (!Objects.equals(this.catList, other.catList)) {
            return false;
        }
        return true;
    }
    
}
