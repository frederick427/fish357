/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.controller;

import heap.fish357.dao.fishDao;
import heap.fish357.model.Author;
import heap.fish357.model.Blog;
import heap.fish357.model.Category;
import heap.fish357.model.Tag;
import heap.fish357.service.fishService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author johnjankeny
 */
@Controller
public class FishBlogController {

    fishService service;

    @Inject
    public FishBlogController(fishService service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homeView(Model model) {
        List<Blog> blogList = service.getRecentBlog();
        Blog blogOne = blogList.get(0);
        Blog blogTwo = blogList.get(1);
        Blog blogThree = blogList.get(2);
        
        

        model.addAttribute("blogOne", blogOne);
        model.addAttribute("blogTwo", blogTwo);
        model.addAttribute("blogThree", blogThree);
        

        return "index";
    }

    @RequestMapping(value = "/displayblog", method = RequestMethod.GET)
    public String displayBlog(Model model, HttpServletRequest request) {
        Blog blog = new Blog();

        String inputValue = request.getParameter("blogID");
        int blogId = Integer.parseInt(inputValue);

        blog = service.getBlogByID(blogId);

        model.addAttribute("blog", blog);

        return "blog";
    }


    @RequestMapping(value = "/returnHome", method = RequestMethod.GET)
    public String returnHome() {
        return "redirect:/displayblog";
    }

    @RequestMapping(value = "/displayAddBlog", method = RequestMethod.GET)
    public String displayAddBlog(Model model) {
        List<Tag> tagList = service.getAllTag();
        List<Category> catList = service.getAllCategory();
        List<Author> authorList = service.getAllAuthor();
        model.addAttribute("authorList", authorList);
        model.addAttribute("tagList", tagList);
        model.addAttribute("catList", catList);

        return "addBlog";
    }

    @RequestMapping(value = "/loadIndex", method = RequestMethod.GET)
    public String loadIndex() {
        return "redirect:/";
    }

    @RequestMapping(value = "/addBlog", method = RequestMethod.POST)
    public String addBlog(HttpServletRequest request) {
        Blog blog = new Blog();
        blog.setTitle(request.getParameter("title"));
        blog.setContent(request.getParameter("content"));
        blog.setViewCount(0);
        blog.setPublishDate(LocalDate.now());
        blog.setUpdateDate(LocalDate.now());

        String authorIdValue = request.getParameterValues("author")[0];
        int authorID = Integer.parseInt(authorIdValue);
        Author author = service.getAuthorByID(authorID);

        blog.setAuthor(author);

        String[] cats = request.getParameterValues("cats");
        List<Category> catList = new ArrayList<>();
        for (String cat : cats) {
            if (cats.length == 0) {
                blog.setCatList(null);
            } else {
                int catID = Integer.parseInt(cat);
                Category confirmedCat = service.getCategoryByID(catID);
                catList.add(confirmedCat);
            }
        }
        blog.setCatList(catList);

        String[] tags = request.getParameterValues("tags");
        List<Tag> tagList = new ArrayList<>();
        for (String tag : tags) {
            if (tags.length == 0) {
                blog.setTagList(null);
            } else {
                int tagID = Integer.parseInt(tag);
                Tag confirmedTag = service.getTagByID(tagID);
                tagList.add(confirmedTag);
            }
        }
        blog.setTagList(tagList);

        service.addPost(blog);

        return "redirect:/";
    }
    @RequestMapping(value = "allBlogs", method = RequestMethod.GET)
    public String allBlogs(Model model){
        List<Blog> blogList = service.getAllBlog();
        model.addAttribute("blogList", blogList);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);
        List<Tag> tagList = service.getAllTag();
        model.addAttribute("tagList", tagList);
        List<Author> authorList = service.getAllAuthor();
        model.addAttribute("authorList", authorList);
        
        return "search";
    }
}
