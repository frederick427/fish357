/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heap.fish357.controller;

import heap.fish357.model.Category;
import heap.fish357.model.Tag;
import heap.fish357.service.fishService;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author davidisakson
 */
@CrossOrigin
@Controller
public class RESTController {
    
    private fishService service;
    
    public RESTController (fishService service){
        this.service = service;
    }
    
    @RequestMapping(value = "/tag/{tagName}", method = RequestMethod.GET)
    @ResponseBody
    public List<Tag> addTag(@PathVariable("tagName") String tagName){
        Tag newTag = new Tag();
        newTag.setTagName(tagName);
        service.addTag(newTag);
        
        return service.getAllTag();
    }
    @RequestMapping(value = "/category/{catName}", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> addCategory(@PathVariable("catName") String catName){
        Category newCat = new Category();
        newCat.setName(catName);
        service.addCategory(newCat);
        
        return service.getAllCategory();
    }
    
    
}
