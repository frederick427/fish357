-- MySQL Script generated by MySQL Workbench
-- Wed Apr  5 15:44:04 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Blog
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Blog
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Blog` DEFAULT CHARACTER SET latin1 ;
USE `Blog` ;

-- -----------------------------------------------------
-- Table `Blog`.`Author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Blog`.`Author` (
  `AuthorID` INT(11) NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(50) NOT NULL,
  `LastName` VARCHAR(50) NOT NULL,
  `Admin` TINYINT(1) NOT NULL DEFAULT '0',
  `DeleteAuthor` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`AuthorID`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Blog`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Blog`.`Category` (
  `CategoryID` INT(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` VARCHAR(50) NOT NULL,
  `DeleteCategory` TINYINT(1) NULL DEFAULT '0',
  `Tag` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`CategoryID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Blog`.`Post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Blog`.`Post` (
  `PostID` INT(11) NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(100) NOT NULL,
  `Content` TEXT NOT NULL,
  `AuthorID` INT(11) NOT NULL,
  `ViewCount` INT(11) NULL DEFAULT NULL,
  `DeletePost` TINYINT(1) NULL DEFAULT '0',
  `Approved` TINYINT(1) NULL DEFAULT '0',
  `PublishDate` DATE NULL DEFAULT NULL,
  `UpdateDate` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`PostID`),
  INDEX `AuthorID` (`AuthorID` ASC),
  CONSTRAINT `blogpost_ibfk_1`
    FOREIGN KEY (`AuthorID`)
    REFERENCES `Blog`.`Author` (`AuthorID`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Blog`.`PostCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Blog`.`PostCategory` (
  `PostID` INT(11) NOT NULL,
  `CategoryID` INT(11) NOT NULL,
  PRIMARY KEY (`PostID`, `CategoryID`),
  INDEX `CategoryID` (`CategoryID` ASC),
  CONSTRAINT `blogpostcategory_ibfk_1`
    FOREIGN KEY (`PostID`)
    REFERENCES `Blog`.`Post` (`PostID`),
  CONSTRAINT `blogpostcategory_ibfk_2`
    FOREIGN KEY (`CategoryID`)
    REFERENCES `Blog`.`Category` (`CategoryID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Blog`.`WebSite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Blog`.`WebSite` (
  `WebID` INT(11) NOT NULL AUTO_INCREMENT,
  `HeaderTitle` TEXT NOT NULL,
  `HeaderSubtitle` TEXT NULL DEFAULT NULL,
  `FooterPrivacy` TEXT NULL DEFAULT NULL,
  `FooterInfo` TEXT NULL DEFAULT NULL,
  `AboutUsBio` TEXT NOT NULL,
  PRIMARY KEY (`WebID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
